CSE.30341.FA17: Project 04
==========================

This is the documentation for [Project 04] of [CSE.30341.FA17].

Members
-------

1. Nick Ribera (nribera@nd.edu)

Design
------

> 1. You will need to implement splitting of free blocks:
>
>   - When should you split a block?
>   I should split when possible when there is enough space in the block for two.
This increases efficiency to minimize wasted space.
>   - How should you split a block?
  I can set the block to a smaller size and free the next block.
Response.

> 2. You will need to implement coalescing of free blocks:
>
>   - When should you coalescing block?
>   When the current and next blocks are both free.
>   - How should you coalesce a block?
    Add the block sizes to each other and update linked list.
Response.

> 3. You will need to implement Next Fit.
>
>   - What information do you need to perform a Next Fit?
>   Block size, process size, and freelist.
>   - How would you implement Next Fit?
    Using pointers, finding first partition to allocate memory block and continuing where it left off.
Response.

> 4. You will need to implement Best Fit.
>
>   - What information do you need to perform a Best Fit?
>   Block size, process size, and freelist.
>   - How would you implement Best Fit?
    Iterate over freelist and find where memory diff is smallest and set current pointer to it.
Response.

> 5. You will need to implement Worst Fit.
>
>   - What information do you need to perform a Worst Fit?
>    Block size, process size, and freelist.
>   - How would you implement Worst Fit?
    Iterate over freelist and find where memory diff is greatest and set current pointer to it.
Response.

> 6. You will need to implement tracking of different information.
>
>   - What information will you want to track?
>    I will need to track each block size, block pointer, freelist, and the numbers of mallocs, frees, reuses, grows, splits, coalesces, blocks, requested, and max heap.
>   - How will you update these trackers?
>    I will implement counters and update the pointer and freelist at each point
>   - How will you report these trackers when the program ends?
    I will use the atexit function.
Response.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].
https://docs.google.com/a/nd.edu/presentation/d/1YQKlB7ZY5fyeYBam_rK7PxpJi4JZqAsa6RPZZBtZbmY/edit?usp=sharing

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 04]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project04.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
