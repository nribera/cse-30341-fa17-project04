CC=       	gcc
CXX= 		g++
CFLAGS= 	-g -gdwarf-2 -std=gnu99 -Wall
LDFLAGS=
LIBRARIES=      lib/libmalloc-ff.so \
		lib/libmalloc-nf.so \
		lib/libmalloc-bf.so \
		lib/libmalloc-wf.so

UNIT_TESTS=	tests/unit_tests/unit_tests

TESTS =	bin/test_00 \
	bin/test_01 \
	bin/test_02 \
	bin/test_03

all:    $(LIBRARIES) $(UNIT_TESTS) $(TESTS)


lib/libmalloc-ff.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DFIT=0 -o $@ $< $(LDFLAGS) -DDEBUG -Wpointer-to-int-cast

lib/libmalloc-nf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DNEXT=0 -o $@ $< $(LDFLAGS) -DDEBUG -Wpointer-to-int-cast

lib/libmalloc-wf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DWORST=0 -o $@ $< $(LDFLAGS) -DDEBUG -Wpointer-to-int-cast

lib/libmalloc-bf.so:     src/malloc.c
	$(CC) -shared -fPIC $(CFLAGS) -DBEST=0 -o $@ $< $(LDFLAGS) -DDEBUG -Wpointer-to-int-cast

bin/test_00: 
	$(CXX) -g ./tests/functional/test_00.c -o ./bin/test_00 -L./lib -DDEBUG

bin/test_01: 
	$(CXX) -g ./tests/functional/test_01.c -o ./bin/test_01

bin/test_02: 
	$(CXX) -g ./tests/functional/test_02.c -o ./bin/test_02

bin/test_03: 
	$(CXX) -g ./tests/functional/test_03.c -o ./bin/test_03
	

unit_tests:
	$(CXX) -g ./tests/unit_tests/unit_tests.cpp -o ./tests/unit_tests/unit_tests -L./lib -lmalloc-bf
	./tests/unit_tests/unit_tests
clean:
	rm -f $(LIBRARIES) $(UNIT_TESTS)

.PHONY: all clean
