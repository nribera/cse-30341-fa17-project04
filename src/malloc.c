/* malloc.c: simple memory allocator -----------------------------------------*/

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>

/* Macros --------------------------------------------------------------------*/

#define ALIGN4(s)           (((((s) - 1) >> 2) << 2) + 4)
#define BLOCK_DATA(b)       ((b) + 1)
#define BLOCK_HEADER(ptr)   ((struct block *)(ptr) - 1)



/* Block structure -----------------------------------------------------------*/

struct block {
    size_t        size;
    struct block *next;
    bool          free;
};

/* Global variables ----------------------------------------------------------*/

struct block *FreeList = NULL;
struct block *last_found = NULL;

/* TODO: Add counters for mallocs, free, reuses, grows */
int exit_callback_registered = 0;
int initial_heap_ptr_calculated = 0;
int num_successful_malloc = 0;
int num_successful_free = 0;
int num_block_reuse = 0;
int num_grows = 0;
int num_splits = 0;
int num_coalesces = 0;
int num_blocks = 0;
int num_requested = 0;
int max_heap = 0;

void stats( ) 
{
  struct block * root = FreeList;
  while( root )
  {
    num_blocks++;
    root = root->next;
  }  

  printf("mallocs: %d\n", num_successful_malloc );
  printf("frees: %d\n", num_successful_free );
  printf("reuses: %d\n", num_block_reuse );
  printf("grows: %d\n", num_grows );
  printf("splits: %d\n", num_splits );
  printf("coalesces: %d\n", num_coalesces );
  printf("blocks: %d\n", num_blocks );
  printf("requested: %d\n", num_requested );
  printf("max heap: %d\n", max_heap );
}

/* Find free block -----------------------------------------------------------*/
struct block *find_free(struct block **last, size_t size) {
    struct block *curr = FreeList;

#if defined FIT && FIT == 0
    /* First fit */
    while (curr && !(curr->free && curr->size >= size)) {
        *last = curr;
        curr  = curr->next;
    }
#endif
#if defined NEXT && NEXT == 0
    /* Next fit */
    curr = last_found;
    while (curr && !(curr->free && curr->size >= size)) {
        *last = curr;
        curr  = curr->next;
    }
#endif
#if defined WORST && WORST == 0 
    /* TODO: Support other policies */
     // Iterate over Freelist find the block where the size diff is greatest
     // set curr to that
    // WORST FIT
    struct block* ptr = NULL;
    int worst_size = -1;
    while (curr) {
      if( curr->free && curr->size >= size && 
        ( curr->size - size ) > worst_size )
      {
        worst_size = curr->size - size;
        ptr = curr;
      }
      curr = curr->next;
    }
    if( ptr != NULL ) return ptr;
#endif
#if defined BEST && BEST == 0 
     // Iterate over Freelist find the block where the size diff is smallest
    // BEST
    struct block* ptr = NULL;
    int best_size = INT_MAX;
    while( curr )
    {
      if( curr->free && curr->size >= size &&
        ( curr->size - size ) < best_size )
      {
        best_size = curr->size - size;
        ptr = curr;
      }
      curr = curr->next;
    }
    if( ptr != NULL ) return ptr;
#endif
    return curr;
}

/* Grow heap -----------------------------------------------------------------*/

static struct block *initial_heap_ptr;
struct block *grow_heap(struct block *last, size_t size) {
    /* Request more space from OS */
    struct block *curr = (struct block *)sbrk(0);
    struct block *prev = (struct block *)sbrk(sizeof(struct block) + size);
printf("Growing the heap to add %d\n", (int)size );
    if( initial_heap_ptr_calculated == 0)
    {
      initial_heap_ptr = (struct block *)sbrk(0);
      initial_heap_ptr_calculated = 1;
    }

    int heap_size = (long long)prev - (long long)initial_heap_ptr ;

    if( heap_size > max_heap ) max_heap = heap_size;

    assert(curr == prev);

    /* OS allocation failed */
    if (curr == (struct block *)-1) {
        return NULL;
    }

    /* Update FreeList if not set */
    if (FreeList == NULL) {
        FreeList = curr;
    }

    /* Attach new block to prev block */
    if (last) {
        last->next = curr;
    }

    /* Update block metadata */
    curr->size = size;
    curr->next = NULL;
    curr->free = true;
  
    num_grows ++;
    return curr;
}

/* Allocate space ------------------------------------------------------------*/

void *malloc(size_t size) {
    if( !exit_callback_registered )
    {
      exit_callback_registered = 1;
      atexit( stats );
    }
    num_requested += size;
    /* Align to multiple of 4 */
    size = ALIGN4(size);
    /* Handle 0 size */
printf("Allocating %d\n", (int)size );
    if (size == 0) {
        return NULL;
    }
    struct block *root = FreeList;
    printf("Current block list: \n");
    while( root )
    {
      printf("Block: at: %p size: %d free: %d next: %p\n", root, root->size, root->free, root->next );
      root = root->next;
    }
    printf("\n");

    /* Look for free block */
    struct block *last = FreeList;
    if( last )
    while( last -> next ) last = last-> next;
    struct block *next = find_free(&last, size);
if( next )
printf("Next: %d Req %d\n", (int)next->size, (int)size );
    if( next && size < next->size )
    {
      // Save the previous size
      int prev_size = next->size;

      // Reset the size to our smaller size
      next->size = size;
printf("Setting new size to %d\n", size );
      next->free = false;

      // Store off where our next pointer points
      struct block *prev_next = next->next;

      // Instead, move our next pointer to just beyond our current pointer
      next->next = (struct block*)((int)next + size);

      // Set our next block to free
      next->next->free = true;
      next->next->size = prev_size - size;
      next->next->next = prev_next;

      // Increment the number of splits
      num_splits++;
    }

    printf("After split block list: \n");
    root = FreeList;
    while( root )
    { 
      printf("Block: at: %p size: %d free: %d next: %p\n", root, root->size, root->free, root->next );
      root = root->next;
    }
    printf("\n");
    /* Could not find free block, so grow heap */
    if (next == NULL) {
printf("Malloc commanding heap to grow\n");
        next = grow_heap(last, size);
        next->free = false;
    }
    else {
      num_block_reuse ++;
    }
    /* Could not find free block or grow heap, so just return NULL */
    if (next == NULL) {
        return NULL;
    }
    
    num_successful_malloc ++;

    return BLOCK_DATA(next);
}

/* Reclaim space -------------------------------------------------------------*/

void free(void *ptr) {
    if (ptr == NULL) {
        return;
    }
    struct block *root = FreeList;
    printf("Free Current block list: \n");
    while( root )
    {
      printf("Block: at: %p size: %d free: %d next: %p\n", root, root->size, root->free, root->next );
      root = root->next;
    }
    printf("\n");
    /* Make block as free */
    struct block *curr = BLOCK_HEADER(ptr);
    assert(curr->free == 0);

    //((struct block*)ptr)->free = true;
    curr->free = true; // THIS LINE IS BAD
    num_successful_free ++;
    root = FreeList;
    while( root )
    {
       // If curr is free and next is free then combine
       if( root && root->next && root->free && root->next->free )
       {
         root->size = root->size + root->next->size;
         root->next = root->next->next;
       }
       root = root->next;
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=cpp: --------------------------------*/
