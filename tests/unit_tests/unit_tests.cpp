#define CATCH_CONFIG_MAIN  
#include "catch.hpp"


TEST_CASE( "Testing atexit" ) {
  bool result = true;
  int *ptr;
  ptr = (int*)malloc( sizeof(int) * 10000 );
  free(ptr);
  ptr = (int*)malloc( sizeof(int));
  free(ptr);
  ptr = (int*)malloc( sizeof(int));
  free(ptr);
  ptr = (int*)malloc( sizeof(int));
  free(ptr);
  ptr = (int*)malloc( sizeof(int));
  free(ptr);
  ptr = (int*)malloc( sizeof(int));
  free(ptr);
  CHECK( result == true );
}
